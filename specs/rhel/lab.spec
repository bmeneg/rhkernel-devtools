%ifarch x86_32
%global pkg_name lab_%{version}_linux_386.tar.gz
%endif
%ifarch x86_64
%global pkg_name lab_%{version}_linux_amd64.tar.gz
%endif

%global gourl	https://github.com/zaquestion/lab

Name:		lab
Version:	0.18.0
Release:	1%{?dist}
Summary:	GitLab cli tool
License:	CC0
URL:		%{gourl}
Source:		%{gourl}/releases/download/v%{version}/%{pkg_name}

Requires:	git

Provides:	bundled(golang(github.com/araddon/dateparse)) = d820a6159ab1e961778690fea995231df707ba0d
Provides:	bundled(golang(github.com/avast/retry-go)) = v1.0.1
Provides:	bundled(golang(github.com/charmbracelet/glamour)) = v0.2.0
Provides:	bundled(golang(github.com/go-git/go-billy)) = v5.0.0
Provides:	bundled(golang(github.com/rivo/tview)) = 82b05c9fb32936cffce7a03c24d891b43a79b82e
Provides:	bundled(golang(github.com/rsteube/carapace)) = v0.1.6
Provides:	bundled(golang(github.com/tcnksm/go-gitconfig)) = v0.1.2
Provides:	bundled(golang(github.com/xanzy/go-gitlab)) = v0.39.0
Provides:	%{_bindir}/%{name}

%description
Lab wraps Git or Hub, making it simple to clone, fork, and interact with
repositories on GitLab.

%prep
%setup -q -c -n %{name}-%{version}

%install
install -m 0755 -vd %{buildroot}%{_bindir}
install -m 0755 -vp %{_builddir}/%{name}-%{version}/lab %{buildroot}%{_bindir}/

%files
%license LICENSE
%doc README.md
%{_bindir}/*

%changelog
* Thu Jan 14 2021 Bruno Meneguele <bmeneg@redhat.com> - 0.18.0-1
- Rebase to the latest official release

* Thu Nov 05 2020 Bruno Meneguele <bmeneg@redhat.com> - 0.17.2-3
- Add "git" as a runtime requirement
- Also add the executable as "provides"

* Wed Nov 04 2020 Bruno Meneguele <bmeneg@redhat.com> - 0.17.2-2
- Add "provides:" with bundled info

* Tue Oct 13 2020 Bruno Meneguele <bmeneg@redhat.com> - 0.17.2-1
- Initial package
