# Red Hat Kernel Developer Tools

This repository keep some files (mostly RPM specfiles) and other information
for the tools that are available at [bmeneguele/rhkernel-devtools COPR
repo](https://copr.fedorainfracloud.org/coprs/bmeneguele/rhkernel-devtools/).

# Bugs

In case you find any packaging related bug, please feel free to open up
issues at the
[issues](https://gitlab.com/ptalbert/rhkernel-devtools/-/issues) menu.
