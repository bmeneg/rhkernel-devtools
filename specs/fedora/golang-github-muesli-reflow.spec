# Generated by go2rpm 1.2
%bcond_without check

# https://github.com/muesli/reflow
%global goipath         github.com/muesli/reflow
Version:                0.2.0

%gometa

%global common_description %{expand:
Reflow lets you word-wrap strings or entire blocks of text. It follows the
io.Writer interface and supports ANSI escape sequences.}

%global golicenses      LICENSE
%global godocs          README.md

Name:           %{goname}
Release:        1%{?dist}
Summary:        Reflow lets you word-wrap strings or entire blocks of text. It follows the io.Writer interface and supports ANSI escape sequences

License:        MIT
URL:            %{gourl}
Source0:        %{gosource}

BuildRequires:  golang(github.com/mattn/go-runewidth)

%description
%{common_description}

%gopkg

%prep
%goprep

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%gopkgfiles

%changelog
* Thu Jan 14 2021 Bruno Meneguele <bmeneg@redhat.com> - 0.2.0-1
- Rebase to upstream release 0.2.0

* Wed Oct 07 13:53:27 CEST 2020 Patrick Talbert <ptalbert@redhat.com> - 0.1.0-1
- Initial package

