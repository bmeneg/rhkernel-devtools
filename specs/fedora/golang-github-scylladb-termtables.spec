# Generated by go2rpm 1.2
%bcond_without check

# https://github.com/scylladb/termtables
%global goipath         github.com/scylladb/termtables
%global commit          c4c0b6d42ff48f1769c3626062d1e3f363f1a662

%gometa

%global common_description %{expand:
Fork of github.com/apcera/termtables.}

%global golicenses      LICENSE
%global godocs          README.md
%global debug_package   %{nil}

Name:           %{goname}
Version:        0
Release:        1%{?dist}
Summary:        Fork of github.com/apcera/termtables

# Upstream license specification: Apache-2.0
License:        ASL 2.0
URL:            %{gourl}
Source0:        %{gosource}

BuildRequires:  golang(github.com/mattn/go-runewidth)

%description
%{common_description}

%gopkg

%prep
%goprep

%build
for cmd in term; do
  %gobuild -o %{gobuilddir}/bin/$(basename $cmd) %{goipath}/$cmd
done

%install
%gopkginstall
install -m 0755 -vd                     %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/* %{buildroot}%{_bindir}/

%if %{with check}
%check
%gocheck
%endif

%files
%license LICENSE
%doc README.md
%{_bindir}/*

%gopkgfiles

%changelog
* Wed Oct 07 15:08:46 CEST 2020 Patrick Talbert <ptalbert@redhat.com> - 0-0.1.20201007gitc4c0b6d
- Initial package

