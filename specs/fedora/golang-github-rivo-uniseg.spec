# Generated by go2rpm 1.2
%bcond_without check

# https://github.com/rivo/uniseg
%global goipath         github.com/rivo/uniseg
Version:                0.1.0

%gometa

%global common_description %{expand:
Unicode Text Segmentation for Go (or: How to Count Characters in a String).}

%global golicenses      LICENSE.txt
%global godocs          README.md

Name:           %{goname}
Release:        1%{?dist}
Summary:        Unicode Text Segmentation for Go (or: How to Count Characters in a String)

License:        MIT
URL:            %{gourl}
Source0:        %{gosource}

%description
%{common_description}

%gopkg

%prep
%goprep

%install
%gopkginstall

%if %{with check}
%check
%gocheck
%endif

%gopkgfiles

%changelog
* Tue Oct 06 15:10:42 CEST 2020 Patrick Talbert <ptalbert@redhat.com> - 0.1.0-1
- Initial package

